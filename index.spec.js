const tmp = require('tmp')
const Serve = require('./index')
const app = require('express')()
const httpCert = require('./src/https-cert')
const axios = require('axios')

app.get('/', (req, res) => {
  res.send('ok')
})

const portfinder = require('portfinder')

test('http', async function (done) {
  let port = await portfinder.getPortPromise();

  const serve = new Serve(app)
  await serve
    .listen(port, cluster => {
      done()
    })
}, 1e4)

test('http worker', async function (done) {
  let port = await portfinder.getPortPromise();

  const serve = new Serve(app)
  serve
    .worker(2)
    .listen(port, cluster => {
      if (cluster.isMaster)
        done()
    })
})

test('https', async function (done) {
  const port = await portfinder.getPortPromise();
  const certDir = tmp.dirSync().name
  const cert = httpCert(certDir, { installCa: false })

  const serve = new Serve(app)
  serve
    .https()
    .listen(port, cluster => {
      done()
    })
}, 1e4)

test('https worker', async function (done) {
  const port = await portfinder.getPortPromise();
  const certDir = tmp.dirSync().name
  const cert = httpCert(certDir, { installCa: false })

  const serve = new Serve(app)
  serve
    .https()
    .worker(2)
    .listen(port, cluster => {
      done()
    })
}, 1e4)