const cluster = require('cluster')
const debug = require('debug')('web-worker')

module.exports = function (str) {
  if (cluster.isMaster)
    return debug(`pid[master]: ${str}`)
  else
    return debug(`pid[${process.pid}]: ${str}`)
} 
