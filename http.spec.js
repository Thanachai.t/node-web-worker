const https = require('https')
const app = require('express')()
const agent = new https.Agent({
  rejectUnauthorized: false
})
const WebServer = require('./src')
const portfinder = require('portfinder')

test('', async function (done) {
  let port = await portfinder.getPortPromise();

  WebServer
    .createServer()
    // .worker(4)
    .https()
    // .httpRedirect(80)
    .listen(port, function () {
      done()
    })
})
