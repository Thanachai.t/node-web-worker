const fs = require('fs')
const path = require('path')
const shell = require('shelljs')
const appDir = path.dirname(require.main.filename)

function initCredentials(certDir, obs) {
  shell.mkdir('-p', certDir)

  obs = Object.assign({ domains: ['127.0.0.1', 'localhost'], installCa: true }, obs)

  let ca
  const keyFilePath = path.resolve(certDir, 'key.pem')
  const certFilePath = path.resolve(certDir, 'cert.pem')

  if (!fs.existsSync(keyFilePath) || !fs.existsSync(certFilePath)) {
    if (!shell.which('mkcert')) {
      console.log(`########################################################################`)
      console.log()
      console.log(`press install https://github.com/FiloSottile/mkcert`)
      console.log()
      console.log(`########################################################################`)
      return
    }
    shell.exec(['mkcert', '-key-file', keyFilePath, '-cert-file', certFilePath, ...obs.domains].join(' '), { silent: true })
    obs.installCa && shell.exec('mkcert -install', { silent: false })
  }
  ca = {
    key: fs.readFileSync(keyFilePath, 'utf8'),
    cert: fs.readFileSync(certFilePath, 'utf8')
  }
}

function getCredentials(certDir) {
  if (fs.existsSync(path.resolve(certDir, 'key.pem')) && fs.existsSync(path.resolve(certDir, 'cert.pem'))) {
    return {
      key: fs.readFileSync(path.resolve(certDir, 'key.pem')),
      cert: fs.readFileSync(path.resolve(certDir, 'cert.pem'))
    };
  } else {
    console.warn(`ssl file not found`)
    return {
      key: fs.readFileSync(path.resolve(__dirname, '../ssl', 'key.pem')),
      cert: fs.readFileSync(path.resolve(__dirname, '../ssl', 'cert.pem'))
    };
  }
}

module.exports = function (certDir, options = {}) {
  options = {
    ...{
      installCa: true
    },
    ...options
  }
  certDir = certDir || path.resolve(appDir, '.cert')
  initCredentials(certDir, options)
  return getCredentials(certDir)
}
