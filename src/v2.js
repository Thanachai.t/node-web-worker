const os = require('os')
const cluster = require('cluster')
const http = require('http')
const https = require('https')
const debug = require('debug')('web_worker')
const httpsCert = require('./https-cert')
const fork = require('./fork')

class WebWorker {
  constructor({
    app,
    httpPort,
    httpsPort,
    httpRedirectPort,
    worker,
    productionWorker,
    initProcess,
    preFork,
    postFork
  } = {}) {
    this._stackCall = []
    this._app = app
    this._httpPort = !Number.isNaN(httpPort) && parseInt(httpPort)
    this._httpsPort = !Number.isNaN(httpsPort) && parseInt(httpsPort)
    this._initProcess = initProcess
    this._preFork = preFork

    if (!Number.isNaN(httpRedirectPort) && httpRedirectPort > 0)
      this._httpRedirectPort = parseInt(httpRedirectPort)

    this._postFork = postFork

    // validate
    {
      if (!this._httpPort && !this._httpRedirectPort && !this._httpsPort) {
        this._httpPort = process.env.PORT || 80
      }
    }

    this.port = this._httpPort || this._httpsPort

    if (cluster.isMaster) {
      if (worker)
        this.worker(worker)

      this.productionWorker(productionWorker)
    }

    if (httpsPort && httpsPort > 0)
      this._isHttps = true
    else
      this._isHttps = false

    if (process.env.NODE_ENV === 'production') {

    } else {

    }

    if (cluster.isMaster)
      debug(`SERVE:${this._httpPort ? ` http:${this._httpPort}` : ``}${httpsPort ? ` https:${httpsPort}` : ``}${this._httpRedirectPort ? ` httpRedirectPort:${this._httpRedirectPort}` : ``}${this._workerTotal ? ` worker:${this._workerTotal}` : ``}`)
  }

  async listen(callback) {
    if (cluster.isMaster) {
      if (this._initProcess)
        await this._initProcess.apply(this)
    }
    await Promise.all(this._stackCall)

    if (cluster.isMaster) {
      if (this._preFork) await this._preFork.apply(this)

      if (this._workerTotal) {
        await fork(this._workerTotal)
        callback && callback.apply(this, cluster)
      }

      if (this._httpRedirectPort) {
        await this.getRedirectServer()
      }

      if (!this._workerTotal) {
        let server = await this.getServer()

        server.listen(this.port, () => {
          debug(`${cluster.isMaster ? 'MASTER' : 'WORKER'}: listen ${this._isHttps ? 'https' : 'http'} port ${this.port}`)

          console.log(`start ${this._isHttps ? 'https' : 'http'} server on port ${this._isHttps ? 'https' : 'http'} port ${this.port}`);
          callback && callback.apply(this, cluster)
        })
      }

      if (this._postFork) await this._postFork.apply(this)
    } else {
      if (this._isHttps && this._httpPort) {
        await http.createServer(this._app)
          .listen(this._httpPort, () => {
            debug(`${cluster.isMaster ? 'MASTER' : 'WORKER'}: listen http port ${this._httpPort}`)
          })
      }
      let server = await this.getServer()

      server.listen(this.port, () => {
        debug(`${cluster.isMaster ? 'MASTER' : 'WORKER'
          }: listen ${this._isHttps ? 'https' : 'http'} port ${this.port}`)

        callback && callback.apply(this, cluster)
        process.send({ pid: process.pid })
      })
    }
  }

  async getServer() {
    if (!this._server) {
      if (typeof this._app == 'function') this._app = await this._app()
      if (this._isHttps && process.env.NODE_ENV !== 'production') {
        this._server = https.createServer(httpsCert(), this._app)
      } else {
        this._server = http.createServer(this._app)
      }
    }

    return this._server
  }

  async getRedirectServer() {
    if (this._httpRedirectPort) {
      const s = http.createServer((req, res) => {
        res.writeHead(302, {
          'Location': `https://${req.headers.host.replace(/:.*/, '')}:${this._httpsPort}${req.url}`
        });
        res.end();
      })
      await new Promise((resolve, reject) => {
        s.listen(this._httpRedirectPort, () => {
          debug(`${cluster.isMaster ? 'MASTER' : 'WORKER'}: listen httpRedirectPort port ${this._httpRedirectPort}`)
          resolve()
        })
      })
    }
  }

  productionWorker(worker) {
    if (process.env.NODE_ENV === 'production') {
      return this.worker(worker)
    } else {
      return this
    }
  }

  worker(worker) {
    if (typeof worker == 'string' && !Number.isNaN(worker))
      worker = parseFloat(worker)

    if (typeof worker === 'function') {
      worker = worker()
    }

    if (worker == null) {
      this._workerTotal = os.cpus().length
    } else if (worker >= 1) {
      this._workerTotal = worker
    } else if (worker < 1) {
      this._workerTotal = Math.floor(os.cpus().length * worker)
    }

    return this
  }
}

module.exports = WebWorker
