const os = require('os')
const cluster = require('cluster')
const http = require('http')
const https = require('https')
const debug = require('debug')('web_worker')
const httpsCert = require('../src/https-cert')

let isFoked
function fork(length = 2) {
  cluster.setupMaster({ silent: true })
  if (cluster.isMaster && !isFoked) {
    return new Promise((resolve, reject) => {
      let arr = []

      for (var i = 0; i < length; i++) {
        var worker = cluster.fork();

        // Receive messages from this worker and handle them in the master process.
        worker.on('message', function (msg) {
          arr.push(msg.pid)
          if (arr.length == length) {
            resolve()
          }
        });
      }
      isFoked = true
    })
  }
}

class WebWorker {
  _stackCall = []

  constructor() { }
  createServer(app) {
    this._app = app
    return this
  }

  productionWorker(worker) {
    if (process.env.NODE_ENV === 'production') {
      return this.worker(worker)
    } else {
      return this
    }
  }

  worker(worker) {
    if (typeof worker === 'function') {
      worker = worker()
    }

    if (!worker) {
      this._workerTotal = os.cpus().length
    } else if (worker >= 1) {
      this._workerTotal = worker
    } else if (worker < 1) {
      this._workerTotal = Math.floor(os.cpus().length * worker)
    }

    return this
  }

  https() {
    this._isHttps = true;
    return this
  }

  listen(port, callback) {
    Promise.all(this._stackCall)
      .then(() => {
        if (cluster.isMaster) {
          if (this._workerTotal) {
            return fork(this._workerTotal)
              .then(callback)
          } else {
            let server = this.getServer()
            if (callback) {
              return server.listen(port || 80, callback)
            } else {
              return new Promise((resolve, reject) => {
                server.listen(port || 80, resolve)
              })
            }
          }
        } else {
          let server = this.getServer()
          server.listen(port || 80, function () {
            callback && callback()
            process.send({ pid: process.pid })
          })
        }
      })
  }

  server(fnc) {
    if ((cluster.isMaster && !this._workerTotal) || (cluster.isWorker)) {
      if (!this._server) {
        this.getServer()
      }

      if (fnc && typeof fnc == 'function') {
        this._stackCall.push(fnc(this._server))
      }
    }
    return this
  }

  getServer() {
    if (!this._server) {
      if (this._isHttps) {
        this._server = https.createServer(httpsCert(), this._app)
      } else {
        this._server = http.createServer(this._app)
      }
    }

    return this._server
  }
}

module.exports = {
  createServer(app) {
    const webWorker = new WebWorker()
    return webWorker.createServer(app)
  }
}