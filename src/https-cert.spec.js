const https = require('https')
const app = require('express')()
const agent = new https.Agent({
  rejectUnauthorized: false
})


test('', async function (done) {
  const cert = await require('./https-cert')()

  let apiPort = await require('portfinder')
    .getPortPromise();

  app.get('/', (req, res) => {
    res.send('ok')
  })

  https.createServer(cert, app)
    .listen(apiPort, async function () {
      const axios = require('axios')
      const agent = new https.Agent({
        rejectUnauthorized: false
      })

      let r = await axios.get('https://127.0.0.1:'+ apiPort, { httpsAgent: agent })
      done()
    })
})