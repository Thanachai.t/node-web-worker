const cluster = require('cluster')
const os = require('os')
const debug = require('../utils/debug')

let isFoked = false

module.exports = function fork() {
  let length = os.cpus().length - 1
  let fnc = () => { }
  if (typeof arguments[0] == 'function') {
    fnc = arguments[0]
  } else if (typeof arguments[0] == 'number') {
    length = arguments[0]
  }

  if (typeof arguments[1] == 'number') {
    length = arguments[1]
  }

  if (cluster.isMaster)
    debug(`fork ${length} cluster`)


  if (process.env.NODE_ENV == 'test') {
    return new Promise(r => r(cluster))
  }

  if (isFoked === false) {
    isFoked = true;
    if (cluster.isMaster) {
      return new Promise((resolve, reject) => {
        let arr = []

        for (var i = 0; i < length; i++) {
          var worker = cluster.fork();

          // Receive messages from this worker and handle them in the master process.
          worker.on('message', function (msg) {
            arr.push(process.pid)
            if (arr.length == length) {
              setTimeout(() => {
                resolve(cluster)
              }, 1)
            }
          });
        }
        isFoked = true
      })
    } else {
      return new Promise(async function (resolve, reject) {
        await fnc()
        process.send(process.pid, err => {
          if (err)
            reject(err)
          else
            resolve(cluster)
        })
      })
    }
  } else {
    return new Promise(r => r(cluster))
  }
}
