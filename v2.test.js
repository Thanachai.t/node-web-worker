const Worker = require('./src/v2')
const portfinder = require('portfinder')
const express = require('express')
const cros = require('cors')
const Axios = require('axios')
const https = require('https')

const app = express()
app.use(cros())
app.get('/', (req, res) => {
  res.send('')
})

const axios = Axios.create({
  // httpsAgent: new https.Agent({
  //   rejectUnauthorized: false
  // })
});

async function request(port, protocal = 'http', amount = 10) {
  return Promise.all(
    new Array(amount).fill(0).map(
      v => axios.get(`${protocal}://127.0.0.1:` + port)
    )
  )
}

beforeEach(() => {
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0
});

describe('http', () => {
  test('worker 0', async (done) => {
    let httpPort = await portfinder.getPortPromise();

    const worker = new Worker({ app: () => app, worker: 0, httpPort })

    worker.listen(async () => {
      await request(httpPort)
      done()
    })
  });

  test('worker 3', async (done) => {
    let httpPort = await portfinder.getPortPromise();

    const worker = new Worker({ app: () => app, worker: 3, httpPort })

    worker.listen(async () => {
      await request(httpPort)
      done()
    })
  });
})

describe('https', () => {
  test('worker 0', async (done) => {
    let httpsPort = await portfinder.getPortPromise();

    const worker = new Worker({ app: () => app, worker: 0, httpsPort })

    worker.listen(async () => {
      await request(httpsPort, 'https')
      done()
    })
  });

  test('worker 3', async (done) => {
    let httpsPort = await portfinder.getPortPromise();

    const worker = new Worker({ app: () => app, worker: 3, httpsPort })

    worker.listen(async () => {
      await request(httpsPort, 'https')
      done()
    })
  });
})

describe('https with http', () => {
  test('worker 0', async (done) => {
    let httpsPort = await portfinder.getPortPromise();
    let httpPort = await portfinder.getPortPromise({
      port: httpsPort + 1
    });

    const worker = new Worker({ app: () => app, worker: 0, httpsPort, httpPort })

    worker.listen(() => {
      done()
    })
  });

  test('worker 3', async (done) => {
    let httpsPort = await portfinder.getPortPromise();
    let httpPort = await portfinder.getPortPromise({
      port: httpsPort + 1
    });

    const worker = new Worker({ app: () => app, worker: 3, httpsPort, httpPort })

    worker.listen(() => {
      done()
    })
  });
})

describe('https with httpRedirectPort', () => {
  test('worker 0', async (done) => {
    let httpsPort = await portfinder.getPortPromise();
    let httpRedirectPort = await portfinder.getPortPromise({
      port: httpsPort + 1
    });

    const worker = new Worker({ app: () => app, worker: 0, httpsPort, httpRedirectPort })

    worker.listen(() => {
      done()
    })
  });

  test('worker 3', async (done) => {
    let httpsPort = await portfinder.getPortPromise();
    let httpRedirectPort = await portfinder.getPortPromise({
      port: httpsPort + 1
    });

    const worker = new Worker({ app: () => app, worker: 3, httpsPort, httpRedirectPort })

    worker.listen(() => {
      done()
    })
  });
})

