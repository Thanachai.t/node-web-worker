const cluster = require('cluster')
const fs = require('fs')
const http = require('http')
const https = require('https')
const os = require('os')
const path = require('path')
const shell = require('shelljs')
const fork = require('./src/fork')
const debug = require('./utils/debug')

const appDir = path.dirname(require.main.filename)

function readCertificate(cert) {
    debug('readCertificate: ' + cert)
    if (typeof cert === 'string') {
        if (/^-----/.test(cert)) {
            return Buffer.from(bufStr, 'utf8')
        } else {
            if (fs.existsSync(cert))
                return fs.readFileSync(cert)
            else
                return null
        }
    }

    return cert
}

class Serve {
    constructor(app, ops) {
        this._app = app
        this._worker = 0
        this._ops = Object.assign({
            port: 443
        }, ops || {})
    }

    https(options = {}) {
        options = {
            ...{
                directory: path.resolve(appDir, '.cert')
            }, ...options
        }

        if (!fs.existsSync(options.directory)) {
            console.warn('WARNING: https ssl directory not exists')
            options.directory = path.resolve(__dirname, '.cert')
        }

        if (!options.key) {
            let key = readCertificate(path.resolve(options.directory, 'key.pem'))
            let cert = readCertificate(path.resolve(options.directory, 'cert.pem'))
            let ca = readCertificate(path.resolve(options.directory, 'ca.pem'))
            this._httpsObs = Object.assign({
                key: key,
                cert: cert,
                ca: ca,
            }, this._httpsObs || {})
        } else {
            let key = readCertificate(options.key)
            let cert = readCertificate(options.cert)
            let ca = readCertificate(options.ca)
            this._httpsObs = Object.assign({
                key: key,
                cert: cert,
                ca: ca,
            }, this._httpsObs || {})
        }

        this._httpRedirectPort = options.httpRedirectPort

        return this
    }

    worker(workerTotal = os.cpus().length) {
        this._worker = workerTotal
        return this
    }

    productionWorker(workerTotal = os.cpus().length) {
        if (process.env.NODE_ENV == 'production')
            this._worker = workerTotal
        return this
    }

    async listen(port, callback = () => { }) {
        if (this._httpsObs) {
            if (this._httpRedirectPort) {
                if (cluster.isMaster) {
                    this._httpServer = http.createServer((req, res) => {
                        res.writeHead(301, { "Location": "https://" + req.headers.host.split(':')[0] + ':' + port + req.url });
                        res.end();
                    })
                    debug(`start http redirect on port ` + this._httpRedirectPort)
                    this._httpServer.listen(this._httpRedirectPort)
                }
            }

            this._httpsServer = https.createServer(this._httpsObs, this._app)
            if (this._worker) {
                await fork(() => {
                    return new Promise((resolve, reject) => {
                        this._httpsServer.listen(port, resolve)
                    })
                }, this._worker)

                debug(`start https server on port ` + port)
                callback(cluster)
            } else {
                debug(`start https server on port ` + port)
                this._httpsServer.listen(port, () => callback(cluster))
            }
        } else {
            this._httpServer = http.createServer(this._app)
            if (this._worker) {
                await fork(() => {
                    return new Promise((resolve, reject) => {
                        this._httpServer.listen(port, () => resolve())
                    })
                }, this._worker)

                debug(`start http server on port ` + port)
                callback(cluster)
            } else {
                debug(`start http server on port ` + port)
                this._httpServer.listen(port, () => callback(cluster))
            }
        }
    }
}

module.exports = Serve
